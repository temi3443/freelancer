source 'https://rubygems.org'

# Core
gem 'rails', '>= 5.0.0.beta3', '< 5.1'
gem 'puma'
gem 'rails-i18n', '>= 5.0.0.beta4', '< 5.1'

# jQuery and friends
gem 'jquery-rails'
gem 'jquery-fileupload-rails', github: 'tors/jquery-fileupload-rails'

# Assets
gem 'compass-rails'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.1'
gem 'jquery-ui-rails'

# Turbolinks
# gem 'turbolinks', '~> 2.5', '>= 2.5.3'
gem 'turbolinks', '>= 5.0.0.beta2', '< 5.1'

# Auth
gem "pundit"
gem "devise", '~> 4.1'

# Rich text editor Quill
# gem 'quill-rails'
gem 'ckeditor'

# For upload file (dropzone)
gem 'dropzonejs-rails'

# Client-side
gem 'judge'
gem "cocoon"

# For photo gallery
# gem 'rails-gallery'
gem 'lightbox2-rails'

# DB stuff
gem 'will_paginate'
gem 'acts-as-taggable-on', :git => 'https://github.com/mbleigh/acts-as-taggable-on', :branch => 'master'
gem 'validates_timeliness', '~> 4.0'
gem 'impressionist'
gem 'ransack', github: 'activerecord-hackery/ransack'
gem 'whenever', require: false
gem 'email_validator', require: 'email_validator/strict'

# Files
gem 'dragonfly'
gem 'dragonfly-s3_data_store'

# Use sidekiq gem for background jobs
gem 'sidekiq'

# For the authorizations with social page
gem 'omniauth'
gem "omniauth-google-oauth2"
gem 'omniauth-facebook'
gem 'omniauth-twitter'
# Solial button sign in
gem 'bootstrap-social-rails'
gem 'font-awesome-rails'

# Statesman for status
gem 'statesman', '~> 2.0', '>= 2.0.1'

# For friendly url
gem 'friendly_id'


group :development, :test do
  gem 'better_errors'
  gem "binding_of_caller"
  gem "letter_opener"
  gem 'byebug'
  gem 'pry-byebug'
  gem 'spring'
  gem 'annotate'
  gem 'sqlite3'
  # For guard
  gem 'guard'
  gem 'guard-bundler', require: false
  gem 'guard-rspec', require: false
  gem 'factory_girl_rails', require: false
  gem 'capybara'
  gem 'launchy'
end

group :production do
  gem 'rack-cache', require: 'rack/cache'
  gem 'pg'
end

#For test
gem "rails-controller-testing", :git => "https://github.com/rails/rails-controller-testing"
gem "rspec-rails", "3.5.0.beta3"
gem 'database_cleaner'
gem 'rspec', '= 3.5.0.beta3'
gem 'rspec-core', '= 3.5.0.beta3'
gem 'rspec-expectations', '= 3.5.0.beta3'
gem 'rspec-mocks', '= 3.5.0.beta3'
gem 'rspec-support', '= 3.5.0.beta3'
gem 'guard-rspec'

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
