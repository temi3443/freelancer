$(document).on 'turbolinks:load', ->
  $('#control_edit_response').click (e) ->
    e.preventDefault()
    $(this).hide()
    $('.content-list_responses').hide()
    response_id = $(this).data('response-id')
    $('form#edit-response-' + response_id).show()

  $('.response').change ->
    $this = $(this)
    if ($this.prop('checked'))
      $this.addClass('current')
      $this.parents('.content-list__my-response').
        find('.response').each (index, elem) ->
          if ($(elem).prop("checked") && !$(elem).hasClass('current') )
            $(elem).prop('checked', false)
      $this.removeClass('current')
      $.ajax
        type: 'POST'
        url: '/responses/accept'
        data:
          id: $(this).data('id')
    else
      $.ajax
        type: 'POST'
        url: '/responses/cancel_response'
        data:
          id: $(this).data('id')
