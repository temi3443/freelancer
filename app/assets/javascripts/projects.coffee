jQuery(document).on 'turbolinks:load', ->
  $('#project_title').blur ->
    judge.validate $('#project_title')[0],
      valid: (element, messages) ->
        element.style.border = '1px solid green'
      invalid: (element, messages) ->
        element.style.border = '2px solid red'
        return
    return

  $('#project_price').blur ->
    judge.validate $('#project_price')[0],
      valid: (element, messages) ->
        element.style.border = '1px solid green'
      invalid: (element, messages) ->
        element.style.border = '2px solid red'
        return
    return

  $('#project_description').blur ->
    judge.validate $('#project_description')[0],
      valid: (element, messages) ->
        element.style.border = '1px solid green'
      invalid: (element, messages) ->
        element.style.border = '2px solid red'
        return
    return

  $('#project_skill_list').blur ->
    judge.validate $('#project_skill_list')[0],
      valid: (element, messages) ->
        element.style.border = '1px solid green'
      invalid: (element, messages) ->
        element.style.border = '2px solid red'
        return
    return

  $("#project_skill_list").select2
    tags: true
    maximumSelectionLength: 15
    tokenSeparators: [',', ' ']



  # $('.response').labelauty
  #   checked_label: "Cancel",
  #   unchecked_label: "Select",
  #   force_random_id: true
