CKEDITOR.editorConfig = function (config) {

  config.toolbar_mini = [
    ["Bold",  "Italic",  "Underline",  "Strike",  "-",  "Subscript",  "Superscript","-", "Link", "-", "NumberedList", "BulletedList"],
  ];
  config.height = 300;
  config.colorButton_colors = '00923E,F8C100,28166F';
  config.language = 'us';
  config.codeSnippet_codeClass = 'contents';

  config.skin = 'flat';
  config.toolbar = 'mini';
  // config.toolbarCanCollapse = true;
  // config.toolbar = "simple";

}
