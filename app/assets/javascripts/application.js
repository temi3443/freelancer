// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require lightbox
//= require jquery_ujs
//= require underscore
//= require judge
//= require lib/select2
//= require jquery-ui/datepicker
//= require cocoon
//= require ckeditor/init
//= require jquery-fileupload/basic
//= require lib/jquery-labelauty
//= require dropzone
//= require freelancers
//= require response
//= require intro
//= require projects
//= require cities
//= require turbolinks
            function showHide(user_panel__menu_id) {

                if (document.getElementById(user_panel__menu_id)) {

                    var obj = document.getElementById(user_panel__menu_id);

                    if (obj.style.height != "240px") {
                        obj.style.height = "240px";
                    }
                    else obj.style.height = "0px";
                }
            }
            $(document).ready(function($) {
                $(".btn_drop").click(function(){
                    $(".btn_drop").toggleClass("open_element");
                });
            });

            // Animation alert message
            function message(){
              $.ajax({
                success: function(){
                  $(".service__message").fadeOut(500);
                }
              });
            }
            setTimeout(message, 4000);

            document.addEventListener("turbolinks:load", function() {



              // For modal windows
              // Get the modal
            if (document.getElementById('myModal')){
              var modal = document.getElementById('myModal');
              var btn = document.getElementById("myBtn");
              var cancel = document.getElementById('cancel');
              var span = document.getElementsByClassName("close")[0];
              var create = document.getElementById('create');
              btn.onclick = function() {
                modal.style.display = "block";
              };
              create.onclick = function() {
                modal.style.display = "none";
              };
              cancel.onclick = function() {
                modal.style.display = "none";
              };
              span.onclick = function() {
                modal.style.display = "none";
              };
              window.onclick = function(event) {
                if (event.target == modal) {
                   modal.style.display = "none";
                }
              };
            }


              if (document.getElementById("link_to_association")){
                var doc_project = document.getElementById("doc_for_project");
                var doc_portfolio = document.getElementById("doc_for_portfolio");
                var docIdProject;
                var docIdPortfolio;
                if(doc_project){
                  docIdProject = doc_project.getAttribute("data-docid");
                } else {
                  docIdPortfolio = doc_portfolio.getAttribute("data-docid");
                }
                document.getElementById("link_to_association").click();
                var attachment_uid = document.getElementsByClassName("d;ropzone");
                Dropzone.autoDiscover = false;
	              var dropzone = new Dropzone (".dropzone", {
		              maxFilesize: 2,
                  paramName: "document[attachment]",
		              addRemoveLinks: false,
                  params:{
                    'project_id': docIdProject,
                    'portfolio_id': docIdPortfolio
                  }
                });
	              dropzone.on("success", function(file) {
	              });
              }

              // For dropdown menu
              var flag;
              $('.dropbtn').click(function(){
                if(flag && flag!=$(this).data('dropid')) document.getElementById('myDropdown-'+ flag).classList.remove("show");
                document.getElementById('myDropdown-'+ $(this).data('dropid')).classList.toggle("show");
                flag = $(this).data('dropid');
              });

              function closeDrop() {
                if (!event.target.matches('.dropbtn')) {
                  var dropdowns = document.getElementsByClassName("dropdown-content");
                  var i;
                  for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains('show')) {
                      openDropdown.classList.remove('show');
                    }
                  }
                }
              }

              // Close the dropdown menu if the user clicks outside of it
              window.onclick = function() {
                closeDrop();
              };

            // Function for render select categories
            var categories_checked, categories_unchecked;
            function categories() {
              categories_checked = [], categories_unchecked = [], index_ch = 0, index_unch = 0;
              $('input:checkbox').each( function(){
                categories_unchecked[index_unch] = this.value;
                index_unch++;
              });
              $('input:checkbox:checked').each( function(){
                categories_checked[index_ch] = this.value;
                index_ch++;
              });
            }

            jQuery(function() {
              $('.select-checkbox').click(function(){
                categories();
                if (categories_checked.length>0){
                  $.ajax({
                    url: '/projects',
                    data: {categories: categories_checked},
                    success: function(result){
                      jQuery('#tasks_list').html(result);
                    }
                  });
                } else {
                  $.ajax({
                    url: '/projects',
                    data: {categories: categories_unchecked},
                    success: function(result){
                      jQuery('#tasks_list').html(result);
                    }
                  });
                }
              });
            });
          });
