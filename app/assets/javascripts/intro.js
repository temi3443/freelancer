document.addEventListener("turbolinks:load", function() {
  // For intro
  if (document.getElementsByClassName('advices')[0]){
    var advice = document.getElementsByClassName('advice');
    var advice_check = document.getElementsByClassName('advice_check');
    var advice_span = document.getElementsByClassName('checkbox__label');
    var advice_button = document.getElementById('advice_check_button');
    $('.advice_check').click(function(){
      var index = this.value;
      function check_next(){
        advice_check[index].disabled = false;
        advice_span[index].className = 'checkbox__label';
      }
      if(index == advice_check.length && this.checked)
        advice_button.disabled = false;
      else
        advice_button.disabled = true;
      if(this.checked){
        advice[index].className = 'advice';
        advice_span[index].className = 'checkbox__label advice_span';
        setTimeout(check_next, 5000);
      } else{
          for (i = index; i < advice.length; i++){
            advice[i].className = 'advice disabled';
            advice_check[i].disabled = true;
            advice_span[index].className = 'checkbox__label';
            advice_check[i].checked = false;
          }
      }
    });
  }
});
