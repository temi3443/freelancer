jQuery(document).on 'turbolinks:load', ->
  $('.birthday1').datepicker
    maxDate: new Date()
    minDate: new Date(1912, 1, 1)
    changeYear: true
    yearRange: "1912:+0"
    changeMonth: true

  $("#freelancer_skill_list").select2
    tags: true
    tokenSeparators: [',']
    maximumSelectionLength: 15

  if $('.form_avatar').size() > 0
    $('.form_avatar').fileupload
      dataType: 'script'
      add: (e, data) ->
        $('.form_avatar .btn_bordered').addClass('loading')
        data.submit()
      change: (e) ->
        $('.form_avatar .btn_bordered').addClass('loading')
      done: (e, data) ->
        if data.result.status is 'success'
          $('.form_avatar .btn_bordered').removeClass('loading')
        else
          $('.form_avatar .btn_bordered').removeClass('loading')

  if $('.form_background').size() > 0
    $('.form_background').fileupload
      dataType: 'script'
      add: (e, data) ->
        $('.form_background .btn_banner').addClass('loading_banner')
        data.submit()
      change: (e) ->
        $('.form_background .btn_banner').addClass('loading_banner')
      done: (e, data) ->
        if data.result.status is 'success'
          $('.form_background .btn_banner').removeClass('loading_banner')
        else
          $('.form_background .btn_banner').removeClass('loading_banner')
