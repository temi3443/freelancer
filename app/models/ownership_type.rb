# == Schema Information
#
# Table name: ownership_types
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class OwnershipType < ApplicationRecord
  has_and_belongs_to_many :freelancers, join_table: :ownership_types_freelancers
end
