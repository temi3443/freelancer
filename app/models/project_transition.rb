# == Schema Information
#
# Table name: project_transitions
#
#  id          :integer          not null, primary key
#  to_state    :string           not null
#  metadata    :text             default({})
#  sort_key    :integer          not null
#  project_id  :integer          not null
#  most_recent :boolean          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_project_transitions_parent_most_recent  (project_id,most_recent) UNIQUE
#  index_project_transitions_parent_sort         (project_id,sort_key) UNIQUE
#

class ProjectTransition < ActiveRecord::Base
  include Statesman::Adapters::ActiveRecordTransition

  belongs_to :project, inverse_of: :project_transitions

  after_destroy :update_most_recent, if: :most_recent?

  private

  def update_most_recent
    last_transition = project.project_transitions.order(:sort_key).last
    return unless last_transition.present?
    last_transition.update_column(:most_recent, true)
  end
end
