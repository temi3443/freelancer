# == Schema Information
#
# Table name: freelancers
#
#  id             :integer          not null, primary key
#  first_name     :string
#  last_name      :string
#  rate           :integer
#  birthday       :string
#  location       :string
#  description    :text
#  site           :string
#  visible        :boolean
#  category_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  specialization :string
#  price_category :string
#  ownership      :string
#  email          :string
#  phone          :string
#  experience     :string
#  username       :string
#  slug           :string
#  intro          :boolean          default(FALSE)
#
# Indexes
#
#  index_freelancers_on_category_id  (category_id)
#  index_freelancers_on_slug         (slug) UNIQUE
#  index_freelancers_on_user_id      (user_id)
#

class Freelancer < ApplicationRecord
  extend FriendlyId
  friendly_id :username, use: :slugged
  belongs_to :category
  belongs_to :user
  has_many :links
  has_many :responses
  has_one :photo
  has_one :background
  has_many :projects, foreign_key: 'performer_id'
  has_many :portfolios
  has_and_belongs_to_many :payment_options, join_table: :payment_options_freelancers
  has_and_belongs_to_many :ownership_types, join_table: :ownership_types_freelancers
  # validates :first_name, :last_name, :description, :specialization, :payment_option_ids, presence: true
  # validates :rate, numericality: { only_integer: true, allow_nil: true }

  accepts_nested_attributes_for :links, allow_destroy: true

  acts_as_taggable
  acts_as_taggable_on :skills

  PRICE_CATEGORIES = [:project, :hour, :month, :for_1000_chars]
  WORK_EXPERIENCE = [:less_than_year, :more_than_year, :more_than_3_years, :more_than_5_years, :more_than_10_years]

  def age
    now = Time.now.utc.to_date
    birthday_date_type = Date.strptime(self.birthday, '%m/%d/%Y')
    now.year - birthday_date_type.year - ((now.month > birthday_date_type.month || (now.month == birthday_date_type.month && now.day >= birthday_date_type.day)) ? 0 : 1 )
  end
end
