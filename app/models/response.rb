# == Schema Information
#
# Table name: responses
#
#  id            :integer          not null, primary key
#  body          :text
#  user_id       :integer
#  project_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  freelancer_id :integer
#  accepted      :boolean
#
# Indexes
#
#  index_responses_on_freelancer_id  (freelancer_id)
#  index_responses_on_project_id     (project_id)
#  index_responses_on_user_id        (user_id)
#

class Response < ApplicationRecord
  belongs_to :user
  belongs_to :freelancer
  belongs_to :project
  has_many :comments

  def days_ago
    now = Date.today
    days = (now - self.created_at.to_date).to_i
  end

end
