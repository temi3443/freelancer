# == Schema Information
#
# Table name: portfolios
#
#  id              :integer          not null, primary key
#  freelancer_id   :integer
#  title           :string
#  description     :text
#  category_id     :integer
#  project_url     :string
#  completion_date :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Portfolio < ApplicationRecord
  belongs_to :category
  has_many :documents, :dependent => :destroy 
  belongs_to :freelancer

  accepts_nested_attributes_for :documents

  validates :title, :description, presence: true
  validates :category_id, presence: true

  acts_as_taggable
  acts_as_taggable_on :skills
  include Impressionist::IsImpressionable
  is_impressionable
end
