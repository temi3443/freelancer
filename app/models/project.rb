# == Schema Information
#
# Table name: projects
#
#  id             :integer          not null, primary key
#  title          :string
#  description    :text
#  price          :integer
#  location       :string
#  payment        :string
#  anonymity      :boolean
#  category_id    :integer
#  archive        :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  price_category :string
#  user_id        :integer
#  performer_id   :integer
#
# Indexes
#
#  index_projects_on_user_id  (user_id)
#

class Project < ActiveRecord::Base
  include Statesman::Adapters::ActiveRecordQueries

  belongs_to :category
  has_many :documents
  belongs_to :user
  has_many :comments
  has_many :responses
  belongs_to :performer, class_name: 'Freelancer', foreign_key: 'performer_id'
  has_and_belongs_to_many :payment_options
  has_many :project_transitions, autosave: false

  accepts_nested_attributes_for :documents

  validates :title, :description, presence: true
  validates :category_id, presence: true
  validates :payment_option_ids, presence: true
  validates :price, numericality: { only_integer: true, allow_nil: true }

  acts_as_taggable
  acts_as_taggable_on :skills
  include Impressionist::IsImpressionable
  is_impressionable
  #is_impressionable :counter_cache => true
  #scope :by_join_date, order("created_at DESC")

  def state_machine
    @state_machine ||= ProjectStateMachine.new(self, transition_class: ProjectTransition)
  end

  def available
    state_machine.current_state == 'available'
  end

  def in_progress
    state_machine.current_state == 'in_progress'
  end

  def completed
    state_machine.current_state == 'completed'
  end

  def has_performer
    state_machine.current_state == 'has_performer'
  end

  def days_ago
    now = Date.today
    (now - self.created_at.to_date).to_i
  end

  def is_owner?(u)
    self.user_id == u.id
  end

  def set_performer(value=0)

  end

  def remove_performer
    update(performer_id: nil)
  end

  def self.transition_class
    ProjectTransition
  end

  def self.initial_state
    :available
  end
end
