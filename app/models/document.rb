# == Schema Information
#
# Table name: documents
#
#  id             :integer          not null, primary key
#  attachment_uid :string
#  title          :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  project_id     :integer
#  portfolio_id   :integer
#
# Indexes
#
#  index_documents_on_project_id  (project_id)
#

class Document < ApplicationRecord
    belongs_to :project
    extend Dragonfly::Model
    dragonfly_accessor :attachment
    # dragonfly_accessor :attachment
end
