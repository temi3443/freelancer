# == Schema Information
#
# Table name: subscribers
#
#  id             :integer          not null, primary key
#  email          :string
#  user_id        :integer
#  subscribe_type :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_subscribers_on_user_id  (user_id)
#

class Subscriber < ApplicationRecord
  has_and_belongs_to_many :categories, join_table: :categories_subscribers
  belongs_to :user
end
