# == Schema Information
#
# Table name: messenger_types
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class MessengerType < ApplicationRecord
  AVAILABLE = %w(Facebook Twitter Skype LinkedIn WhatsApp FacebookMessenger Website GitHub BitBucket StackOverflow Quora Foursquare YouTube Flickr)
  has_many :links

  validates :title, inclusion: { in: AVAILABLE }, uniqueness: true
end
