# == Schema Information
#
# Table name: backgrounds
#
#  id            :integer          not null, primary key
#  banner_uid    :string
#  user_id       :integer
#  freelancer_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_backgrounds_on_freelancer_id  (freelancer_id)
#  index_backgrounds_on_user_id        (user_id)
#

class Background < ApplicationRecord
  extend Dragonfly::Model
  dragonfly_accessor :banner do
    default  Rails.root.join('public', 'images', 'default', 'banner_2_large.png')
  end
end
