# == Schema Information
#
# Table name: links
#
#  id                :integer          not null, primary key
#  url               :string
#  freelancer_id     :integer
#  messenger_type_id :integer
#
# Indexes
#
#  index_links_on_freelancer_id      (freelancer_id)
#  index_links_on_messenger_type_id  (messenger_type_id)
#

class Link < ApplicationRecord
  belongs_to :freelancer
  belongs_to :messenger_type
end
