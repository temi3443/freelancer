# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Category < ActiveRecord::Base
  has_many :projects
  has_many :freelancers
  has_and_belongs_to_many :subscribers, join_table: :categories_subscribers
end
