# == Schema Information
#
# Table name: photos
#
#  id            :integer          not null, primary key
#  avatar_uid    :string
#  user_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  freelancer_id :integer
#  banner_uid    :string
#
# Indexes
#
#  index_photos_on_freelancer_id  (freelancer_id)
#  index_photos_on_user_id        (user_id)
#

class Photo < ApplicationRecord
  dragonfly_accessor :avatar do
    default  Rails.root.join('public', 'images', 'default', 'avatar.png')
  end


  validates :avatar, presence: true
  validates_size_of :avatar, maximum: 500.kilobytes,
                    message: "should be no more than 500 KB", if: :avatar_changed?

  validates_property :format, of: :avatar, in: [:jpeg, :jpg, :gif, :png], case_sensitive: false,
                     message: "should be either .jpeg, .jpg, gif, .png", if: :avatar_changed?

  belongs_to :freelancer
  belongs_to :user
end
