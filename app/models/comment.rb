# == Schema Information
#
# Table name: comments
#
#  id          :integer          not null, primary key
#  body        :text
#  response_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#
# Indexes
#
#  index_comments_on_response_id  (response_id)
#

class Comment < ApplicationRecord
  belongs_to :response
  belongs_to :project
  validates :body, presence: true
end
