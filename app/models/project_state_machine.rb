class ProjectStateMachine
  include Statesman::Machine

  # Define all possible states
  state :available, initial: :true
  state :has_performer
  state :in_progress
  state :completed

  # Define transition rules
  transition from: :available, to: :has_performer
  transition from: :has_performer, to: [:available, :in_progress]
  transition from: :in_progress, to: :completed
end
