# == Schema Information
#
# Table name: payment_options
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PaymentOption < ApplicationRecord
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :freelancers, join_table: :payment_options_freelancers
end