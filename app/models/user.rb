# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  username               :string
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#  index_users_on_username              (username) UNIQUE
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_one :freelancer, dependent: :destroy
  has_many :projects
  has_many :responses
  has_one :subscriber
  has_one :intro
  has_many :authorizations
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable, :confirmable, :lockable, :omniauthable, omniauth_providers: [:facebook, :google_oauth2, :twitter]

  validates :username, presence: true, uniqueness: true
  validates :email, presence: true, uniqueness: true, email: true


  before_validation :ensure_username_has_a_value
  after_create :prepare_photo_and_freelancer

  private

  def ensure_username_has_a_value
    if self.username.nil?
      self.username = self.email unless email.blank?
    end
  end

  def self.find_for_oauth(auth)
    authorization = Authorization.where(provider: auth.provider, uid: auth.uid.to_s).first
    if authorization
      return authorization.user
    end
    if auth.info[:email]
      email = auth.info[:email]
    end
    username = auth.info[:name]
    user = User.where(email: email).first
    if user
      user.authorizations.create(provider: auth.provider, uid: auth.uid)
    else
      password = Devise.friendly_token[0, 20]
      user = User.create!(email: email, password: password, password_confirmation: password,
                                                  username: username, confirmed_at: Time.now)
    end

    user
  end

  def prepare_photo_and_freelancer
    fl = Freelancer.create(user_id: self.id, email: self.email, username: self.username)
    Photo.create(user_id: self.id, freelancer_id: fl.id)
    Background.create(user_id: self.id, freelancer_id: fl.id)
    Subscriber.create(user_id: self.id, subscribe_type: 2)
  end
end
