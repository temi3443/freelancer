module FreelancersHelper
  def work_experience_options
    Freelancer::WORK_EXPERIENCE.map {|val| [t("dropdowns.work_experience.#{val}"), val] }
  end
end