module ApplicationHelper
  def url_with_protocol(url)
    /^http/i.match(url) ? url : "http://#{url}"
  end

  def header(text)
    render 'shared/header', text: text.html_safe
  end

  def aside(content)
    render 'shared/aside', content: content.html_safe
  end

  def avatar_for(obj)
    image_tag obj.avatar.url, class: "avatario", alt: 'Avatar'
  end

  def background_for(obj)
    image_tag obj.banner.url, class: "banner-img", alt: 'BannerImg'
  end

  def price_category_options
    Freelancer::PRICE_CATEGORIES.map {|val| [t("dropdowns.price_categories.#{val}"), val] }
  end
end
