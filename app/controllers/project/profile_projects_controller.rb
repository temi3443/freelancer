class Project::ProfileProjectsController < ApplicationController
  def index
    if current_user
      @projects = Project.where(user_id: current_user.id)
      render 'profile_projects/index'
    end
  end
end
