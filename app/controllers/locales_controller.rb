class LocalesController < ApplicationController
  def new
    session[:freelance_de_locale] = params[:locale]
    redirect_back fallback_location: root_path
  end
end