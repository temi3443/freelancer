class ResponsesController < ApplicationController
  before_action :set_freelancer, only: [:create, :update]
  before_action :set_response, only: [:edit, :update, :destroy, :accept, :cancel_response]

  def index
    if user_signed_in?
      @responses = Response.where(freelancer_id: @freelancer.id).
          paginate(page: params[:page], per_page: 10)
    end
  end

  def show
    @comment = @response.comment.build
  end

  def create
    @project = Project.find(params[:project_id])
    @response = @project.responses.create(response_params.merge(freelancer_id: @freelancer.id ))
  end

  def edit
  end

  def update
    @response.update(response_params)
    @project = @response.project
  end

  def destroy
    @project = @response.project
    locals project: @project
    @response.destroy
  end

  def accept
    @freelancer = @response.freelancer
    @project = @response.project
    @project.state_machine.transition_to(:has_performer)
    @project.update( performer_id: @response.freelancer_id )
    ResponseMailer.send_mail(@response).deliver_now
    respond_to do |format|
      format.js { render 'projects/accept' }
    end

  end

  def cancel_response
    @freelancer = @response.freelancer
    @project = @response.project
    @project.state_machine.transition_to(:available)
    @project.remove_performer
    ResponseMailer.cancel_response(@response).deliver_now
    respond_to do |format|
      format.js { render 'projects/accept' }
    end
  end


  private

  def set_response
    @response = Response.find_by(id: params[:id])
  end

  def set_freelancer
    if current_user
      @freelancer = Freelancer.find_by(user_id: current_user.id)
    end
  end

  def response_params
    params.require(:response).permit(:body, :project_id, :user_id)
  end
end
