class DocumentsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def destroy
    @document = Document.find_by(id: params[:id])
    @document.destroy
  end

def new
  @document = Document.new
end

def create
  # binding.pry
  @document = Document.new(upload_params)
  @document.title = params[:document].require(:attachment).original_filename
  @document.project_id = params[:project_id]
  @document.portfolio_id = -1
  if @document.save
  end
end

private
def upload_params
  params.require(:document).permit(:attachment)
end

end
