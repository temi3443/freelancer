class BackgroundsController < ApplicationController
  before_action :set_banner, only: [:update, :destroy]
  skip_before_action :verify_authenticity_token

  def update
    @background.update(banner_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @background = Background.find(params[:id])
    @background.banner = File.new('public/images/default/banner_2_large.png')
    if @background.save
      respond_to do |format|
        format.js { render 'backgrounds/update' }
      end
    end
  end

  private

  def banner_params
    params.require(:background).permit(:banner)
  end

  def set_banner
    @freelancer = Freelancer.friendly.find(params[:id])
    @background = @freelancer.background
  end
end
