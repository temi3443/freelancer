class PortfoliosController < ApplicationController

  def new
    @portfolio = Portfolio.new
  end

  def watch
    @portfolio = Portfolio.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
  end

  def create
    @portfolio = Portfolio.create(portfolio_params)
    if @portfolio.save
      @doc = Document.where(portfolio_id: -1)
      @doc.each do |d|
        d.portfolio_id = @portfolio.id
        d.save
      end
      redirect_to :back
      flash[:error] = "Portfolio update"
    else
      flash[:error] = "Could not update portfolio"
      redirect_to :back
    end
  end

  def edit
    @portfolio = Portfolio.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    @portfolio = Portfolio.find(params[:id])
    @portfolio.update(portfolio_params)
    redirect_to :back
    flash[:error] = "Portfolio update"
  end

  def destroy
    @portfolio = Portfolio.find(params[:id])
    @portfolio.destroy
    redirect_to :back
    flash[:error] = "Portfolio update"
  end

  private

  def portfolio_params
    params.require(:portfolio).permit(:title, :description, :completion_date, :project_url,
                                    :category_id, :freelancer_id, { skill_list: [] },  :skill,
                                    { skill_ids: [] }, :skill_ids, documents_attributes: [:attachment, :_destroy])
  end

end
