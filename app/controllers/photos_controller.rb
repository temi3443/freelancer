class PhotosController < ApplicationController
  before_action :set_photo, only: [:update, :destroy]

  def update
    @photo.update(photo_params)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @photo.avatar = File.new('public/images/default/avatar.png')
    if @photo.save
      respond_to do |format|
        format.js { render 'photos/update' }
      end
    end
  end

  private

  def photo_params
    params.require(:photo).permit(:avatar)
  end

  def set_photo
    @freelancer = Freelancer.friendly.find(params[:id])
    @photo = @freelancer.photo
  end
end
