class CommentsController < ApplicationController
  def create
    @response = Response.find(params[:response_id])
    @comment = @response.comments.create(comment_params)
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :user_id)

  end
end
