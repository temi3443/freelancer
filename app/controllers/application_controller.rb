class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout :layout_by_resource


  before_action :get_current_freelancer, if: :user_signed_in?
  before_action :get_current_avatar, if: :user_signed_in?
  before_action :set_global_search_variable
  before_action  :store_location
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale


  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end


  protected


  def layout_by_resource
    devise_controller? ? "devise" : "application"
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end

  private

  def get_current_freelancer
    @freelancer = Freelancer.find_by(user_id: current_user.id)
  end

  def get_current_avatar
    @photo = Photo.find_by(user_id: current_user.id)
  end

  def locals(action = nil, hash)
    render action: action, locals: hash
  end

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
    flash[:warning] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default
    redirect_to request.referrer || root_path
  end

  def set_global_search_variable
    @q = Project.search(params[:q])
  end

  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    return unless request.get?
    if request.path != "/users/sign_in" &&
        request.path != "/users/sign_up" &&
        request.path != "/users/password/new" &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation" &&
        request.path != "/users/sign_out" &&
        !request.xhr? # don't store ajax calls
      session[:previous_url] = request.fullpath
    end
  end

  def set_locale
    if session[:freelance_de_locale]
      loc = session[:freelance_de_locale].to_sym
    else
      loc = I18n.default_locale
      session[:freelance_de_locale] = loc
    end
    I18n.locale = I18n.locale_available?(loc) ? loc : I18n.default_locale
  end
end
