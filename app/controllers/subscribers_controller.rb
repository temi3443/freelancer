class SubscribersController < ApplicationController
    after_action :verify_authorized, only: [:update]
    before_action :set_subscriber, only: [:update, :edit]

  def edit
    if @subscriber.present?
      authorize @subscriber
    else
      skip_authorization
    end
  end

  def update
    if @subscriber.present?
      authorize @subscriber
    else
      skip_authorization
    end
    if @subscriber.update(subscriber_params)
      redirect_to @subscriber.user.freelancer
      flash[:notice] = "Subscribe update"
    end
  end

  private

  def subscriber_params
    params.require(:subscriber).permit(:email, :subscribe_type, category_ids: [])
  end

  def set_subscriber
    @subscriber = Subscriber.find_by(id: params[:id])
  end
  
end
