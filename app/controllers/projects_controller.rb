class ProjectsController < ApplicationController
  require 'will_paginate/array'
  before_action :set_project, only: [:show, :edit, :update]
  before_action :authenticate_user!, only: [:new, :edit]
  before_action :set_freelancer, only: [:show, :new]
  before_action :set_response_current_user, only: [:show], if: :exist_response?
  after_action :verify_authorized, only: [:update]

  impressionist actions: [:show]

  def index
    if params[:categories]
      @projects = Project.where(category_id: [params[:categories]])
                                  .order("projects.created_at DESC")
                                  .paginate(page: params[:page], per_page: 10)
      render @projects
    elsif params[:skill]
      @projects = Project.tagged_with(params[:skill]).
        paginate(page: params[:page], per_page: 10)
    else
      @q = Project.ransack(params[:q])
      @projects = @q.result(distinct: true)
                    .order("projects.created_at DESC")
                    .paginate(page: params[:page], per_page: 10)
    end

    @categories = Category.all
  end

  def new
    @freelancer.update_attribute :intro, true
    @project = Project.new
    locals payment_options: PaymentOption.all
  end

  def create
    @project = Project.create(project_params)
    if @project.save
      flash[:notice] = 'Your project successfully created.'
      redirect_to @project
    else
      flash[:error] = "Could not save project"
      locals :new, payment_options: PaymentOption.all
    end
  end

  def show
    @new_response = @project.responses.build
  end

  def edit
    if @project.present?
      authorize @project
    else
      skip_authorization
    end
    locals payment_options: PaymentOption.all
  end

  def archive
    @project = Project.find(params[:id])
    if !@project.archive
      @project.update_attribute('archive', true)
      @response = Response.where(project_id: @project.id, freelancer_id: @project.performer_id).first
      if @project.performer.present?
        ResponseMailer.cancel_response(@response).deliver_now
      end
    else
     @project.update_attribute('archive', false)
    end
    redirect_to :back
  end

  def show_archive
    if current_user
      @projects = Project.where(user_id: current_user.id)
      render 'projects/archive'
    end
  end

  def destroy
    @project = Project.find(params[:id])
    if !@project.archive
      @project.update_attribute('archive', true)
    else
     @project.update_attribute('archive', false)
    end
    redirect_to :back
  end

  def update
    if @project.present?
      authorize @project
    else
      skip_authorization
    end
    if @project.update(project_params)
      redirect_to @project
    else
      locals :edit, payment_options: PaymentOption.all
    end
  end

  def search
    index
    render :index
  end

  def category
    @category = Category.find( params[:id])
    @projects = @category.projects.order("projects.created_at DESC")
  end

  def upload_photo
    @project = Project.find(params[:id])
    @document = Document.create(:image => params[:image])
    render :json => { :status => @document.valid? }
  end

  def accept
    @project = Project.find(params[:id])
    @project.state_machine.transition_to(:in_progress)
    respond_to do |format|
      format.js { render 'projects/accept' }
    end
  end

  def refuse
    @project = Project.find(params[:id])
    @project.state_machine.transition_to(:available)
    respond_to do |format|
      format.js { render 'projects/accept' }
    end
  end

  def completed
    @project = Project.find(params[:id])
    @project.state_machine.transition_to(:completed)
    @project.update_attribute('archive', true)
    respond_to do |format|
      format.js { render 'projects/accept' }
    end
  end

  def intro

  end

  private

  def exist_response?
    if user_signed_in?
      Response.exists?(freelancer_id: @freelancer.id, project_id: @project.id)
    else
      false
    end
  end

  def set_freelancer
    @freelancer = Freelancer.find_by(user_id: current_user.id) if user_signed_in?
  end

  def set_project
    @project = Project.find_by(id: params[:id])
  end

  def set_response_current_user
    @response = Response.find_by(project_id: @project.id, freelancer_id: @freelancer.id)
  end

  def project_params
    params.require(:project).permit(:title, :description, :price, :location,
                                    :anonymity, :price_category, :category_id, :user_id, { skill_list: [] },  :skill,
                                    { skill_ids: [] }, :skill_ids, documents_attributes: [:attachment, :_destroy], payment_option_ids: [])
  end
end
