class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    @user = User.find_for_oauth(request.env['omniauth.auth'])
    if @user.persisted?
      sign_in( @user )
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      redirect_to :controller => 'projects', :action => 'index'
    end
  end

  def google_oauth2
    @user = User.find_for_oauth(request.env['omniauth.auth'])
    if @user.present?
      sign_in( @user )
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
      redirect_to :controller => 'projects', :action => 'index'    end
  end

  def twitter
    @user = User.find_for_oauth(request.env['omniauth.auth'])
    if @user.persisted?
      sign_in( @user )
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Twitter"
      redirect_to :controller => 'freelancers', :action => 'edit', :id => @user.freelancer.id
    end
  end
end
