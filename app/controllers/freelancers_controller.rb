class FreelancersController < ApplicationController
  before_action :set_freelancer, only: [:show, :edit, :update]
  before_action :set_variables, only: [:show, :edit, :update]
  before_action :authenticate_user!, only: [:edit]

  after_action :verify_authorized, only: [:update]

  def index
    if params[:skill]
      @freelancers = Freelancer.tagged_with(params[:skill]).paginate(page: params[:page], per_page: 10)
    else
      @freelancers = Freelancer.all.paginate(page: params[:page], per_page: 10)
    end
    @categories = Category.all
  end

  def show
  end

  def edit
    if @freelancer.present?
      authorize @freelancer
    else
      skip_authorization
    end
  end

  def update
    if @freelancer.present?
      authorize @freelancer
    else
      skip_authorization
    end
    redirect_to @freelancer if @freelancer.update(freelancer_params)
  end

  private

  def freelancer_params
    params.require(:freelancer).permit(:first_name, :last_name, :rate, :birthday,
                                       :location,  :description, :site, :visible, :avatar, :specialization, :price_category,
                                       :ownership, :email, :phone, :experience, :skill_list, { skill_list: [] }, :skill, { skill_ids: [] }, :skill_ids, :category_id,
                                       links_attributes: [:id, :url, :messenger_type_id, :_destroy], payment_option_ids: [], ownership_type_ids: [])
  end

  def set_freelancer
    @freelancer = Freelancer.friendly.find(params[:id])
  end

  def set_variables
    @photo = @freelancer.photo
    @background = @freelancer.background
    @ownership_types = OwnershipType.all
    @payment_options = PaymentOption.all
  end

end
