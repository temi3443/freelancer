class StaticPagesController < ApplicationController
  def about
  end

  def changeslog
  end

  def help
  end

  def feedback
  end
end
