class SubscriptionMailer < ApplicationMailer

  def send_email(projects, user_id)
    @user = User.find(user_id)
    @projects = projects
    mail to: @user.email, subject: 'New project for day'
  end
end
