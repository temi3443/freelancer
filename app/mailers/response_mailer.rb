class ResponseMailer < ApplicationMailer
  def send_mail(response)
   @freelancer = Freelancer.find(response.freelancer_id)
   @project = Project.find(response.project_id)
    mail to: @freelancer.email, subject: "You have been chosen as the performer on the project #{@project.title}"
  end

  def cancel_response(response)
    @freelancer = Freelancer.find(response.freelancer_id)
    @project = Project.find(response.project_id)
    mail to: @freelancer.email, subject: "Client canceled you as the performer on the project #{@project.title}"
  end
end
