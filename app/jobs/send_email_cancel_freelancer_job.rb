class SendEmailCancelFreelancerJob < ApplicationJob
  queue_as :default

  def perform(response, project)
    ResponseMailer.cancel_response(response, project).deliver_later
  end
end
