class SendMailSelectFreelancerJob < ApplicationJob
  queue_as :default

  def perform(response, project)
    ResponseMailer.send_mail(response, project).deliver_later
  end
end
