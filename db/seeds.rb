# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#
# categories = Category.create([{ name: 'Website-Entwicklung'}, { name: 'Mobile Anwendungen'}, { name: 'Software-Entwicklung'},
#                               { name: 'Content'}, { name: 'Administration'}, { name: 'Design und Multimedia'},
#                               { name: 'Polygraphie'}, { name: 'Engineering'}, { name: 'Werbung und Marketing'}, { name: 'Verschiedenes'}])
#
# ownership_types = OwnershipType.create([{title: "company"}, {title: "individual"}])

%w(Facebook Twitter Skype LinkedIn WhatsApp FacebookMessenger Website GitHub BitBucket StackOverflow Quora Foursquare YouTube Flickr).each do |title|
 MessengerType.create!({title: title})
end



# payment_options = PaymentOption.create([{ name: "cash" }, { name: "bank transfer" }, { name: "e-money" }])
#@index = 56

#while @index < 60 do
#  user = User.create({id: @index, email: "test#{@index}@mail.com", username: "username#{@index}", confirmed_at: Time.now, password: 'qwerty', password_confirmation: 'qwerty'})
#  project = Project.create({id: @index, title: "Project #{@index}" , description: "Description for #{@index} project", anonymity: false, user_id: 30, created_at: Time.now, price_category: 'project'})
#  @index = @index + 1
#end
