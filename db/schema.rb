# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160825113740) do

  create_table "authorizations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_authorizations_on_user_id"
  end

  create_table "backgrounds", force: :cascade do |t|
    t.string   "banner_uid"
    t.integer  "user_id"
    t.integer  "freelancer_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["freelancer_id"], name: "index_backgrounds_on_freelancer_id"
    t.index ["user_id"], name: "index_backgrounds_on_user_id"
  end

  create_table "banners", force: :cascade do |t|
    t.string   "banner_uid"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_banners_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories_subscribers", force: :cascade do |t|
    t.integer "category_id"
    t.integer "subscriber_id"
    t.index ["category_id"], name: "index_categories_subscribers_on_category_id"
    t.index ["subscriber_id"], name: "index_categories_subscribers_on_subscriber_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text     "body"
    t.integer  "response_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.index ["response_id"], name: "index_comments_on_response_id"
  end

  create_table "documents", force: :cascade do |t|
    t.string   "attachment_uid"
    t.string   "title"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "project_id"
    t.integer  "portfolio_id"
    t.index ["project_id"], name: "index_documents_on_project_id"
  end

  create_table "freelancers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "rate"
    t.string   "birthday"
    t.string   "location"
    t.text     "description"
    t.string   "site"
    t.boolean  "visible"
    t.integer  "category_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "user_id"
    t.string   "specialization"
    t.string   "price_category"
    t.string   "ownership"
    t.string   "email"
    t.string   "phone"
    t.string   "experience"
    t.string   "username"
    t.string   "slug"
    t.boolean  "intro",          default: false
    t.index ["category_id"], name: "index_freelancers_on_category_id"
    t.index ["slug"], name: "index_freelancers_on_slug", unique: true
    t.index ["user_id"], name: "index_freelancers_on_user_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index"
    t.index ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index"
    t.index ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index"
    t.index ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index"
    t.index ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index"
    t.index ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index"
    t.index ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index"
    t.index ["user_id"], name: "index_impressions_on_user_id"
  end

  create_table "links", force: :cascade do |t|
    t.string  "url"
    t.integer "freelancer_id"
    t.integer "messenger_type_id"
    t.index ["freelancer_id"], name: "index_links_on_freelancer_id"
    t.index ["messenger_type_id"], name: "index_links_on_messenger_type_id"
  end

  create_table "messenger_types", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ownership_types", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ownership_types_freelancers", force: :cascade do |t|
    t.integer "freelancer_id"
    t.integer "ownership_type_id"
    t.index ["freelancer_id"], name: "index_ownership_types_freelancers_on_freelancer_id"
    t.index ["ownership_type_id"], name: "index_ownership_types_freelancers_on_ownership_type_id"
  end

  create_table "payment_options", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_options_freelancers", force: :cascade do |t|
    t.integer "freelancer_id"
    t.integer "payment_option_id"
    t.index ["freelancer_id"], name: "index_payment_options_freelancers_on_freelancer_id"
    t.index ["payment_option_id"], name: "index_payment_options_freelancers_on_payment_option_id"
  end

  create_table "payment_options_projects", force: :cascade do |t|
    t.integer "project_id"
    t.integer "payment_option_id"
    t.index ["payment_option_id"], name: "index_payment_options_projects_on_payment_option_id"
    t.index ["project_id"], name: "index_payment_options_projects_on_project_id"
  end

  create_table "photos", force: :cascade do |t|
    t.string   "avatar_uid"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "freelancer_id"
    t.string   "banner_uid"
    t.index ["freelancer_id"], name: "index_photos_on_freelancer_id"
    t.index ["user_id"], name: "index_photos_on_user_id"
  end

  create_table "portfolios", force: :cascade do |t|
    t.integer  "freelancer_id"
    t.string   "title"
    t.text     "description"
    t.integer  "category_id"
    t.string   "project_url"
    t.string   "completion_date"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "project_transitions", force: :cascade do |t|
    t.string   "to_state",                   null: false
    t.text     "metadata",    default: "{}"
    t.integer  "sort_key",                   null: false
    t.integer  "project_id",                 null: false
    t.boolean  "most_recent",                null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["project_id", "most_recent"], name: "index_project_transitions_parent_most_recent", unique: true, where: "most_recent"
    t.index ["project_id", "sort_key"], name: "index_project_transitions_parent_sort", unique: true
  end

  create_table "projects", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "price"
    t.string   "location"
    t.string   "payment"
    t.boolean  "anonymity"
    t.integer  "category_id"
    t.boolean  "archive",        default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "price_category"
    t.integer  "user_id"
    t.integer  "performer_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "responses", force: :cascade do |t|
    t.text     "body"
    t.boolean  "accepted"
    t.integer  "user_id"
    t.integer  "project_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "freelancer_id"
    t.index ["freelancer_id"], name: "index_responses_on_freelancer_id"
    t.index ["project_id"], name: "index_responses_on_project_id"
    t.index ["user_id"], name: "index_responses_on_user_id"
  end

  create_table "subscribers", force: :cascade do |t|
    t.string   "email"
    t.integer  "user_id"
    t.integer  "subscribe_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["user_id"], name: "index_subscribers_on_user_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tagger_type"
    t.integer  "tagger_id"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "username"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
