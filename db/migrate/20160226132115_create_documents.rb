class CreateDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|
      t.string :attachment_uid
      t.string :title

      t.timestamps
    end
  end
end
