class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :title
      t.text :description
      t.integer :price
      t.string :skill
      t.string :location
      t.string :payment
      t.boolean :anonymity
      t.string :file
      t.references :category, index: true, foreign_key: true

      t.timestamps
    end
  end
end
