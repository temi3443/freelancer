class CreateBackgrounds < ActiveRecord::Migration[5.0]
  def change
    create_table :backgrounds do |t|
      t.string :banner_uid
      t.belongs_to :user, index: true,  foreign_key: true
      t.belongs_to :freelancer, index: true, foreign_key: true

      t.timestamps
    end
  end
end
