class DeleteIndexFromProjects < ActiveRecord::Migration[5.0]
  def change
    remove_index :projects, :category_id
  end
end
