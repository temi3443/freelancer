class BirthdayChangeColumnTypeFromFreelancers < ActiveRecord::Migration[5.0]
  def change
    change_column :freelancers, :birthday, :string
  end
end
