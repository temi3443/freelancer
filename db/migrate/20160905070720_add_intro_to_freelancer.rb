class AddIntroToFreelancer < ActiveRecord::Migration[5.0]
  def change
    add_column :freelancers, :intro, :boolean, default: false
  end
end
