class CreateFreelancers < ActiveRecord::Migration[5.0]
  def change
    create_table :freelancers do |t|
      t.string :first_name
      t.string :last_name
      t.integer :rate
      t.date :birthday
      t.string :location
      t.text :description
      t.string :site
      t.boolean :visible
      t.string :avatar
      t.references :category, index: true, foreign_key: true

      t.timestamps
    end
  end
end
