class AddProjectIdToDocuments < ActiveRecord::Migration[5.0]
  def change
    add_column :documents, :project_id, :integer
    add_index :documents, :project_id
  end
end
