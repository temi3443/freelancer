class AddFreelancerIdToResponses < ActiveRecord::Migration[5.0]
  def change
    add_column :responses, :freelancer_id, :integer
    
    add_index :responses, :freelancer_id
  end
end
