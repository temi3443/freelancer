class CreateMessengerTypes < ActiveRecord::Migration[5.0]
  def change
    unless table_exists?(:messenger_types)
      create_table :messenger_types do |t|
        t.string :title
       
        t.timestamps
      end
    end
  end
end
