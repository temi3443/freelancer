class AddSubscribeType < ActiveRecord::Migration[5.0]
  def change
    create_table :subscribers, force: :cascade do |t|
      t.string   "email"
      t.integer  "user_id"
      t.integer  "subscribe_type"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["user_id"], name: "index_subscribers_on_user_id"
    end

  end
end
