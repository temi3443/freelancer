class CreatePaymentOptionsFreelancers < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_options_freelancers do |t|
      t.belongs_to :freelancer, index: true
      t.belongs_to :payment_option, index: true
    end
  end
end
