class AddOwnershipToFreelancers < ActiveRecord::Migration[5.0]
  def change
    add_column :freelancers, :ownership, :string
  end
end
