class AddPhoneToFreelancers < ActiveRecord::Migration[5.0]
  def change
    add_column :freelancers, :phone, :string
  end
end
