class AddEmailToFreelancers < ActiveRecord::Migration[5.0]
  def change
    add_column :freelancers, :email, :string
  end
end
