class AddProjectStatus < ActiveRecord::Migration[5.0]
  def change
    create_table :projects, force: :cascade do |t|
      t.string   "title"
      t.text     "description"
      t.integer  "price"
      t.string   "location"
      t.string   "payment"
      t.boolean  "anonymity"
      t.integer  "category_id"
      t.boolean  "project_status", default: false
      t.datetime "created_at",     null: false
      t.datetime "updated_at",     null: false
      t.string   "price_category"
      t.integer  "user_id"
      t.integer  "performer_id"
      t.index ["user_id"], name: "index_projects_on_user_id"
    end
  end
end
