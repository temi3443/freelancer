class CreateCategoriesSubscribers < ActiveRecord::Migration[5.0]
  def change
    create_table :categories_subscribers do |t|
      t.belongs_to :category, index: true
      t.belongs_to :subscriber, index: true
    end
  end
end
