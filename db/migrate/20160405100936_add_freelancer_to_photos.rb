class AddFreelancerToPhotos < ActiveRecord::Migration[5.0]
  def change
    add_reference :photos, :freelancer, foreign_key: true
  end
end
