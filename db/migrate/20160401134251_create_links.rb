class CreateLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :links do |t|
      t.string :url
      t.references :freelancer, foreign_key: true
      t.references :messenger_type, foreign_key: true
    end
  end
end
