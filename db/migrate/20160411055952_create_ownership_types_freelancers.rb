class CreateOwnershipTypesFreelancers < ActiveRecord::Migration[5.0]
  def change
    create_table :ownership_types_freelancers do |t|
      t.belongs_to :freelancer, index: true
      t.belongs_to :ownership_type, index: true
    end
  end
end
