class AddPerfomerToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :performer_id, :integer
  end
end
