class AddPriceCategoryToProject < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :price_category, :string
  end
end
