class AddPortfolioidToDocument < ActiveRecord::Migration[5.0]
  def change
    add_column :documents, :portfolio_id, :integer
  end
end
