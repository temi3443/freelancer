class AddPriceCategoryToFreelancers < ActiveRecord::Migration[5.0]
  def change
    add_column :freelancers, :price_category, :string
  end
end
