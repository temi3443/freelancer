class AddDocumentIdToProject < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :document_id, :integer
  end
end
