class FixProjectStatus < ActiveRecord::Migration[5.0]
  def change
    rename_column :projects, :project_status, :archive
  end
end
