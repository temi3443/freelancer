class DeleteDocumentIdFromProjects < ActiveRecord::Migration[5.0]
  def change
    remove_column :projects, :document_id
  end
end
