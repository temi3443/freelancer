class CreatePaymentOptionsProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_options_projects do |t|
      t.belongs_to :project, index: true
      t.belongs_to :payment_option, index: true
    end
  end
end
