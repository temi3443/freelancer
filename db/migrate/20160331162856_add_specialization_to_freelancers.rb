class AddSpecializationToFreelancers < ActiveRecord::Migration[5.0]
  def change
    add_column :freelancers, :specialization, :string
  end
end
