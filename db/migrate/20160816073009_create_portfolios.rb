class CreatePortfolios < ActiveRecord::Migration[5.0]
  def change
    create_table :portfolios do |t|
      t.integer :freelancer_id
      t.string :title
      t.text :description
      t.integer :category_id
      t.string :project_url
      t.string :completion_date

      t.timestamps
    end
  end
end
