class AddUsernameToFreelancer < ActiveRecord::Migration[5.0]
  def change
    add_column :freelancers, :username, :string
    add_column :freelancers, :slug, :string
    add_index :freelancers, :slug, unique: true
  end
end
