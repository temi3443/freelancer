FactoryGirl.define do
  factory :project do
    title "TestTitle"
    description "TestDescription"
    created_at Date.today
    updated_at Date.today
  end

  factory :invalid_project, class: "Project" do
    title nil
    description nil
  end
end
