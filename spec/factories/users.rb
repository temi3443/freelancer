FactoryGirl.define do
  sequence :email do |n|
    "user#{n}@test.com"
  end

  sequence :username do |n|
    "user#{n}"
  end

  sequence :id do |n|
   n
  end

  factory :user do
    id
    username
    email
    password 'qwerty'
    password_confirmation 'qwerty'
    confirmed_at Date.today

    factory :user_with_freelancer do
      after(:create) do |user|
        freelancer = create(:freelancer, user: user, id: user.id)
        create(:photo, freelancer: freelancer, id: freelancer.id)
      end
    end
  end

  factory :freelancer do

  end


  factory :photo do

  end

  factory :payment_options do

  end

  factory :payment_option do

  end

  factory :payment_option_cash do
  end


end
