require 'rails_helper'

feature 'create new project', %q{ user can crate new project} do

  scenario 'authenticate user can create new project' do
    User.create!(email: 'tst0@mail.com', password: '111111', username: 'rest0', confirmed_at: Date.today)

    visit new_user_session_path
    fill_in 'E-mail', with: 'tst0@mail.com'
    fill_in 'Password', with: '111111'
    click_on 'Sign in'
    visit new_project_path
    fill_in 'project[title]', with: 'Test'
    fill_in 'project[description]', with: 'TestDescription'

    click_on 'Create'

    expect(page).to have_content 'Your project successfully created.'
  end

  scenario 'authenticate user cannot create new project' do
    User.create!(email: 'tst1@mail.com', password: '111111', username: 'rest1', confirmed_at: Date.today)

    visit new_user_session_path
    fill_in 'E-mail', with: 'tst1@mail.com'
    fill_in 'Password', with: '111111'
    click_on 'Sign in'
    visit new_project_path
    fill_in 'project[title]', with: 'Test'

    click_on 'Create'

    expect(page).to_not have_content 'Your project successfully created.'
  end

end
