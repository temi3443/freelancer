require 'rails_helper'

feature 'edit user profle', %q{ freelancer or client
        can edit your profile account} do

  given(:user) { create(:user_with_freelancer) }



  scenario 'authenticate user can edit your profile' do
    sign_in user


    visit edit_freelancer_path(user.id)

    fill_in 'First name', with: 'Ivan'
    fill_in 'Last name', with: 'Ivanov'
    fill_in 'Rate', with: 10
    select 'Project', from: 'freelancer_price_category'
    save_and_open_page
    check('cash')
    check('bank transfer')
    uncheck('e-money')
    fill_in 'Birthday', with: '13.02.1982'
    fill_in 'Location', with: 'Moscow'
    fill_in 'freelancer_description', with: 'about me'
    fill_in 'freelancer_specialization', with: 'Ruby on rails'
    fill_in 'freelancer_skill_list', with: 'ruby, javascript'
    select 'less than a year', from: 'freelancer_experience'
    select 'Content', from: 'freelancer_category_id'
    check 'company'
    uncheck 'individual'

    fill_in 'Home page', with: 'namesite.com'
    fill_in 'Phone', with: '+74256895645'
    fill_in 'Email', with: 'mail@mail.com'

    click_on 'Add'
    select 'Site', from: 'messenger_type_id'
    fill_in 'url', with: 'site.com'

    check('Show my profile in the general list of freelancers')

    click_on 'Publish'

    expect(page).to have_link('Settings')

  end

  scenario 'authenticate user try edit another profile ' do
  end

  scenario 'non authenticate user try edit your profile' do
  end

  scenario 'non authenticate user try edit another profile' do
  end

end
