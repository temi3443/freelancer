require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  include Devise::Test::ControllerHelpers
  include Devise::TestHelpers

  let(:project) { FactoryGirl.create(:project, user_id: 1) }
  let(:invalid_project) { FactoryGirl.create(:invalid_project) }
  let(:response) { FactoryGirl.create(:response) }
  let(:user) { FactoryGirl.create(:user, id: 1) }
  before {
    sign_in user
  }


# For index
  describe "GET #index" do
      let(:projects) { FactoryGirl.create_list(:project, 2) }
      before { get :index }
    it 'populates an array of all @projects' do
      expect(assigns(:projects)).to match_array(projects)
    end
    it 'renders index view' do
      expect(response).to render_template :index
    end
  end

# For new
  describe "GET #new" do
    before {
      get :new
    }
    it 'assigns the new @project' do
      expect(assigns(:project)).to be_a_new(Project)
    end
    it 'renders index view' do
      get :index
      expect(response).to render_template :index
    end
  end

# For edit
  describe "GET #edit" do
    before {
      get :edit, params: { id: project}
    }
    it 'assigns the requested project to @project' do
      expect(assigns(:project)).to eq(project)
    end
    it 'renders edit view' do
      expect(response).to render_template :edit
    end
  end

  # For create
  describe 'POST #create' do
    context 'with valid attributes' do
      it 'saves the new project in the database' do
        expect { post :create, project: FactoryGirl.attributes_for(:project) }.to change(Project, :count).by(1)
      end

      it 'redirects to new project view' do
        post :create, project: FactoryGirl.attributes_for(:project)
        expect(response).to redirect_to project_path(assigns(:project))
      end
    end
    context 'with invalid attributes' do
      it 'does not save the project' do
        expect { post :create, project: FactoryGirl.attributes_for(:invalid_project) }.to_not change(Project, :count)
      end

      it 're-renders new view' do
        post :create, project: FactoryGirl.attributes_for(:invalid_project)
        expect(response).to render_template :new
      end
    end
  end

  # For show
  describe 'GET #show' do
   before { get :show, id: project }

   it 'assings the requested project to @project' do
     expect(assigns(:project)).to eq project
   end

   it 'renders show view' do
     expect(response).to render_template :show
   end
 end

  # For update
  describe 'PATCH #update' do
    context 'valid update attributes' do
      it 'assings the requested project to @project' do
        patch :update, id: project, project: FactoryGirl.attributes_for(:project)
        expect(assigns(:project)).to eq project
      end

      it 'changes project attributes' do
        patch :update, id: project, project: { title: 'title update', description: 'update'}
        project.reload
        expect(project.title).to eq 'title update'
        expect(project.description).to eq 'update'
      end

      it 'redirects to the updated project' do
        patch :update, id: project, project: FactoryGirl.attributes_for(:project)
        expect(response).to redirect_to project
      end
    end

    context 'invalid attributes' do
      before { patch :update, id: project, project: { title: 'new title', description: nil} }

      it 'does not change project attributes' do
        project.reload
        expect(project.title).to eq 'TestTitle'
        expect(project.description).to eq 'TestDescription'
      end

      it 're-renders edit view' do
        expect(response).to render_template :edit
      end
    end
  end

# For show_archive
  describe 'GET #show_archive' do
    before {get :show_archive }

    it 'render archive view' do
      expect(response).to render_template :archive
    end
  end

# For destroy
  describe 'DELETE #destroy' do
    before { project }

    it 'deletes project' do
      expect { delete :destroy, id: project }.to change(Project, :count).by(0)
    end

    it 'redirect to index view' do
      delete :destroy, id: project
      expect(response).to render_template :archive
    end
end
end
