module FeatureMacros
  def sign_in(user)
    visit new_user_session_path
    fill_in 'E-mail', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Sign in'
  end
  
  def sign_up
    
  end
end