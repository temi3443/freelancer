require 'rails_helper'

RSpec.describe Project, :type => :model do
  it 'validates new project' do
    expect(Project.new(title: 'test')).to_not be_valid
    expect(Project.new(title: 'test',description: 'hello', category_id: 2)).to be_valid
    expect(Project.new(title: 'test2',description: '')).to_not be_valid
  end
end
