namespace :subscription do
  desc 'Send dayly mails with new projects'
  task :send_mails => :environment do

    Subscriber.where.not(categories: []).find_each do |subscriber|
      projects = Project.where(category_id: subscriber.categories, created_at: 1.days.ago..DateTime.now)
      if subscriber.subscribe_type==1 && !projects.blank?
        user_id = subscriber.user_id
        SubscriptionMailer.send_email(projects, user_id).deliver!
      end
    end
  end
end

namespace :subscription do
  desc 'Send hourly mails with new projects'
  task :send_hourly_mails => :environment do

    Subscriber.where.not(categories: []).find_each do |subscriber|
      projects = Project.where(category_id: subscriber.categories, created_at: 1.hours.ago..DateTime.now)
      if subscriber.subscribe_type==0 && !projects.blank?
        user_id = subscriber.user_id
        SubscriptionMailer.send_email(projects, user_id).deliver!
      end
    end
  end
end
