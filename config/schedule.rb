set :environment, "development"
set :output, "#{path}/log/cron.log"

every 1.day :at => '9:00 am'  do
  rake "subscription:send_mails"
end

every 1.hours do
  rake "subscription:send_hourly_mails"
end
