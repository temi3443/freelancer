Rails.application.routes.draw do
  get 'about', to: 'static_pages#about'
  get 'changeslog', to: 'static_pages#changeslog'
  get 'help', to: 'static_pages#help'
  get 'feedback', to: 'static_pages#feedback'

  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  resources :locales, only: [:new]


  get 'documents/delete'
  get '/projects/archive'
  get '/projects/show_archive'
  get "portfolios/watch" => 'portfolios#watch', :as => :watch
  get 'projects/accept' => 'projects#accept', :as => :accept
  get 'projects/refuse' => 'projects#refuse', :as => :refuse
  get 'projects/completed' => 'projects#completed', :as => :completed
  get 'projects/accept_intro' => 'projects#accept_intro', :as => :accept_intro
  get 'projects/intro' => 'projects#intro', :as => :intro

  resources :freelancers
  resources :projects do
    resources :responses, expect: :index
  end
  resources :project do
    resources :comments
  end
  resources :responses do
    resources :comments
  end

  resources :intros
  resources :documents
  resources :photos
  resources :portfolios
  resources :backgrounds
  resources :project_transitions
  namespace :project do
    get 'my/tasks', to: 'profile_projects#index', as: :user_projects
  end
  resources :subscribers
  get 'tags/:skill', to: 'projects#index', as: :skill
  get 'freelancer_skills/:skill', to: 'freelancers#index', as: :freelancer_skill
  get 'my/responses', to: 'responses#index'
  post 'responses/accept', to: 'responses#accept'
  post 'responses/cancel_response', to: 'responses#cancel_response'

  resources :projects do
    collection do
      match 'search' => 'projects#search', via: [:get, :post], as: :search
    end
  end

  mount Judge::Engine => '/judge'

  root 'projects#index'
end
